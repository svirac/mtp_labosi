function [outData, outMean, outSigma] = standardizeData(inData, inMean, inSigma)

nCols = length(inData(1,:));
nRows = length(inData(:,1));

outData = zeros(nRows,nCols);

% Standardize data if mean and sigma are not passed to the function
if nargin < 2
    
    outMean = zeros(1,nCols);
    outSigma = zeros(1,nCols);

    % Calculate mean and standard deviation
    for i=1:nCols
        % Calculate mean
        sum = 0;
        for j=1:nRows
            if isnan(inData(j,i))
                continue
            end
            sum = sum + inData(j,i);
        end
        outMean(i) = sum/nRows;

        % Calculate standard deviation
        num = 0;
        for j = 1:nRows
            num = num + square(inData(j,i) - outMean(i))^2;
        end
        outSigma(i) = sqrt(num / (nRows-1));
    end

    % Standardize data
    for i=1:nCols
        for j=1:nRows
            outData(j,i) = (inData(j,i)-outMean(i)) / outSigma(i);
        end
    end

% Standardize data with mean and sigma passed to the function
else
    for i=1:nCols
        for j=1:nRows
            outData(j,i) = (inData(j,i)-inMean(i)) / inSigma(i);
        end
    end
    outMean = inMean;
    outSigma = inSigma;
end
end
