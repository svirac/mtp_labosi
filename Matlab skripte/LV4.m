close all;
clear;
clc;

%% Load data
filename = 'data/pima-indians-diabetes.csv';
dataset = csvread(filename);
n = length(dataset);

% Find missing data
missing_ind = [];
for i=1:n
    for j=2:8
        if(dataset(i,j) == 0)
            missing_ind = [missing_ind, i];
        end
    end
end

% Dataset after removing missing data
dataset(missing_ind,:) = [];
n = length(dataset);
A = dataset(:,1);   % Number of times pregnant
B = dataset(:,2);   % Plasma glucose concentration a 2 hours in an oral glucose tolerance test
C = dataset(:,3);   % Diastolic blood pressure (mm Hg)
D = dataset(:,4);   % Triceps skin fold thickness (mm)
E = dataset(:,5);   % 2-Hour serum insulin (mu U/ml
F = dataset(:,6);   % Body mass index (weight in kg/(height in m)^2)
G = dataset(:,7);   % Diabetes pedigree function
H = dataset(:,8);   % Age (years)
Y = dataset(:,9);   % Class variable (0 or 1)

% Group data
dataset0=[];dataset1=[];
A0=[];B0=[];C0=[];D0=[];E0=[];F0=[];G0=[];H0=[];
A1=[];B1=[];C1=[];D1=[];E1=[];F1=[];G1=[];H1=[];
for i=1:n
    if Y(i) == 0
        dataset0=[dataset0
                  dataset(i,:)];
        A0 = [A0, A(i)];
        B0 = [B0, B(i)];
        C0 = [C0, C(i)];
        D0 = [D0, D(i)];
        E0 = [E0, E(i)];
        F0 = [F0, F(i)];
        G0 = [G0, G(i)];
        H0 = [H0, H(i)];
    else
        dataset1=[dataset1
                  dataset(i,:)];
        A1 = [A1, A(i)];
        B1 = [B1, B(i)];
        C1 = [C1, C(i)];
        D1 = [D1, D(i)];
        E1 = [E1, E(i)];
        F1 = [F1, F(i)];
        G1 = [G1, G(i)];
        H1 = [H1, H(i)];
    end
end
dataset0(:,9) = [];
dataset1(:,9) = [];

%% Histograms
% Feature 1
figure;
histogram(A0,10,'Normalization','probability');
hold on;
histogram(A1,10,'Normalization','probability');
title('Feature 1');
ylabel('Ratio');
xlabel('Number of times pregnant');
legend('0','1');

% Feature 2
figure;
histogram(B0,10,'Normalization','probability');
hold on;
histogram(B1,10,'Normalization','probability');
title('Feature 2');
ylabel('Ratio');
xlabel('Plasma glucose concentration');
legend('0','1');

% Feature 3
figure;
histogram(C0,10,'Normalization','probability');
hold on;
histogram(C1,10,'Normalization','probability');
title('Feature 3');
ylabel('Ratio');
xlabel('Diastolic blood pressure (mm Hg)');
legend('0','1');

% Feature 4
figure;
histogram(D0,10,'Normalization','probability');
hold on;
histogram(D1,10,'Normalization','probability');
title('Feature 4');
ylabel('Ratio');
xlabel('Triceps skin fold thickness (mm)');
legend('0','1');

% Feature 5
figure;
histogram(E0,10,'Normalization','probability');
hold on;
histogram(E1,10,'Normalization','probability');
title('Feature 5');
ylabel('Ratio');
xlabel(' 2-Hour serum insulin (mu U/ml)');
legend('0','1');

% Feature 6
figure;
histogram(F0,10,'Normalization','probability');
hold on;
histogram(F1,10,'Normalization','probability');
title('Feature 6');
ylabel('Ratio');
xlabel('Body mass index (kg/m^2)');
legend('0','1');

% Feature 7
figure;
histogram(G0,10,'Normalization','probability');
hold on;
histogram(G1,10,'Normalization','probability');
title('Feature 7');
ylabel('Ratio');
xlabel('Diabetes pedigree function');
legend('0','1');

% Feature 8
figure;
histogram(H0,10,'Normalization','probability');
hold on;
histogram(H1,10,'Normalization','probability');
title('Feature 8');
ylabel('Ratio');
xlabel('Age (years)');
legend('0','1');


%% Empirical cumulative distribution function
% 1st and 3rd quartile
p = [0.25 0.75];

% Feature 1
q_A0 = quantile(A0,p);
q_A1 = quantile(A1,p);
figure; ecdf(A0);
hold on; ecdf(A1);
hold on; plot(q_A0,p,'b*');
hold on; plot(q_A1,p,'r*');
title('Feature 1 ecdf');
legend('0','1','location','best');
xlabel('Number of times pregnant');
ylabel('Ratio');

% Feature 2
q_B0 = quantile(B0,p);
q_B1 = quantile(B1,p);
figure; ecdf(B0);
hold on; ecdf(B1);
hold on; plot(q_B0,p,'b*');
hold on; plot(q_B1,p,'r*');
title('Feature 2 ecdf');
legend('0','1','location','best');
xlabel('Plasma glucose concentration a 2 hours in an oral glucose tolerance test');
ylabel('Ratio');

% Feature 3
q_C0 = quantile(C0,p);
q_C1 = quantile(C1,p);
figure; ecdf(C0);
hold on; ecdf(C1);
hold on; plot(q_C0,p,'b*');
hold on; plot(q_C1,p,'r*');
title('Feature 3 ecdf');
legend('0','1','location','best');
xlabel('Diastolic blood pressure (mm Hg)');
ylabel('Ratio');

% Feature 4
q_D0 = quantile(D0,p);
q_D1 = quantile(D1,p);
figure; ecdf(D0);
hold on; ecdf(D1);
hold on; plot(q_D0,p,'b*');
hold on; plot(q_D1,p,'r*');
title('Feature 4 ecdf');
legend('0','1','location','best');
xlabel('Triceps skin fold thickness (mm)');
ylabel('Ratio');

% Feature 5
q_E0 = quantile(E0,p);
q_E1 = quantile(E1,p);
figure; ecdf(E0);
hold on; ecdf(E1);
hold on; plot(q_E0,p,'b*');
hold on; plot(q_E1,p,'r*');
title('Feature 5 ecdf');
legend('0','1','location','best');
xlabel('2-Hour serum insulin (mu U/ml)');
ylabel('Ratio');

% Feature 6
q_F0 = quantile(F0,p);
q_F1 = quantile(F1,p);
figure; ecdf(F0);
hold on; ecdf(F1);
hold on; plot(q_F0,p,'b*');
hold on; plot(q_F1,p,'r*');
title('Feature 6 ecdf');
legend('0','1','location','best');
xlabel('Body mass index (weight in kg/(height in m)^2)');
ylabel('Ratio');

% Feature 7
q_G0 = quantile(G0,p);
q_G1 = quantile(G1,p);
figure; ecdf(G0);
hold on; ecdf(G1);
hold on; plot(q_G0,p,'b*');
hold on; plot(q_G1,p,'r*');
title('Feature 7 ecdf');
legend('0','1','location','best');
xlabel('Diabetes pedigree function');
ylabel('Ratio');

% Feature 8
q_H0 = quantile(H0,p);
q_H1 = quantile(H1,p);
figure; ecdf(H0);
hold on; ecdf(H1);
hold on; plot(q_H0,p,'b*');
hold on; plot(q_H1,p,'r*');
title('Feature 8 ecdf');
legend('0','1','location','best');
xlabel('Age (years)');
ylabel('Ratio');


%% Boxplots
% Normalize data
dataset0(:,1) = normalizeData(dataset0(:,1));
dataset0(:,2) = normalizeData(dataset0(:,2));
dataset0(:,3) = normalizeData(dataset0(:,3));
dataset0(:,4) = normalizeData(dataset0(:,4));
dataset0(:,5) = normalizeData(dataset0(:,5));
dataset0(:,6) = normalizeData(dataset0(:,6));
dataset0(:,7) = normalizeData(dataset0(:,7));
dataset0(:,8) = normalizeData(dataset0(:,8));

dataset1(:,1) = normalizeData(dataset1(:,1));
dataset1(:,2) = normalizeData(dataset1(:,2));
dataset1(:,3) = normalizeData(dataset1(:,3));
dataset1(:,4) = normalizeData(dataset1(:,4));
dataset1(:,5) = normalizeData(dataset1(:,5));
dataset1(:,6) = normalizeData(dataset1(:,6));
dataset1(:,7) = normalizeData(dataset1(:,7));
dataset1(:,8) = normalizeData(dataset1(:,8));

colNames = {'Times pregnant','Plasma glucose conc.',...
            'Diastolic blood pressure','Triceps skin thickness',...
            '2-Hour serum insulin','BMI','Diabetes pedigree func.','Age'};
% Class 0
figure;
boxplot(dataset0,'orientation','horizontal','labels',colNames);
title('Does not have diabetes');

% Class 1
figure;
boxplot(dataset1,'orientation','horizontal','labels',colNames);
title('Has diabetes');


%% Scatter plots
group = dataset(:,9);
% Scatter 2-1
figure;
scatterhist(dataset(:,2),dataset(:,1),'Group',group,'Marker','..');
title('Scatter 2-1');
xlabel('Plasma glucose concentration');
ylabel('Number of times pregnant');

% Scatter 2-3
figure;
scatterhist(dataset(:,2),dataset(:,3),'Group',group,'Marker','..');
title('Scatter 2-3');
xlabel('Plasma glucose concentration');
ylabel('Diastolic blood pressure');

% Scatter 2-4
figure;
scatterhist(dataset(:,2),dataset(:,4),'Group',group,'Marker','..');
title('Scatter 2-4');
xlabel('Plasma glucose concentration');
ylabel('Triceps skin fold thickness');

% Scatter 2-5
figure;
scatterhist(dataset(:,2),dataset(:,5),'Group',group,'Marker','..');
title('Scatter 2-5');
xlabel('Plasma glucose concentration');
ylabel('2-Hour serum insulin');

% Scatter 2-6
figure;
scatterhist(dataset(:,2),dataset(:,6),'Group',group,'Marker','..');
title('Scatter 2-6');
xlabel('Plasma glucose concentration');
ylabel('Body mass index');

% Scatter 7-1
figure;
scatterhist(dataset(:,7),dataset(:,1),'Group',group,'Marker','..');
title('Scatter 7-1');
xlabel('Diabetes pedigree function');
ylabel('Number of times pregnant');

% Scatter 7-3
figure;
scatterhist(dataset(:,7),dataset(:,3),'Group',group,'Marker','..');
title('Scatter 7-3');
xlabel('Diabetes pedigree function');
ylabel('Diastolic blood pressure');

% Scatter 7-4
figure;
scatterhist(dataset(:,7),dataset(:,4),'Group',group,'Marker','..');
title('Scatter 7-4');
xlabel('Diabetes pedigree function');
ylabel('Triceps skin fold thickness');

% Scatter 7-5
figure;
scatterhist(dataset(:,7),dataset(:,5),'Group',group,'Marker','..');
title('Scatter 7-5');
xlabel('Diabetes pedigree function');
ylabel('2-Hour serum insulin');

% Scatter 7-6
figure;
scatterhist(dataset(:,7),dataset(:,6),'Group',group,'Marker','..');
title('Scatter 7-6');
xlabel('Diabetes pedigree function');
ylabel('Body mass index');







