clear;
close all;
clc;

%% Load data
filename= 'data/identData_complete.csv';
% Data got from LV1 - without ANs and outliers
data = csvread(filename);
t = data(:,1);
u = data(:,2);
y = data(:,3);
ts = 0.2;
n = length(y);

data_id = iddata(y, u, ts);
% Detrend
radna_tocka = 1.5;
data_T = iddata(y-radna_tocka, u-u(1), ts);

% Divide data
[trainInd, valInd, testInd] = divideblock(n,0.7,0.15,0.15);
dataTrain = data_T(trainInd);
dataVal = data_T(valInd);
dataTest = data_T(testInd);


%% ARX
A = 1:5;
B = 1:5;
K = 1:5;
n_models = length(A)*length(B)*length(K);
models = cell(1,n_models);
fitVals = zeros(1,n_models);
params = zeros(n_models, 3);
count = 1;
for i = 1:length(A)
    for j = 1:length(B)
        for k = 1:length(K)
            
            params(count,:) = [A(i) B(j) K(k)];
            models{count} = arx(dataTrain, params(count,:));           
            [valY, valFit] = compare(dataVal,models{count});
            fitVals(count) = valFit;
            
            count = count+1;
        end
    end
end

[~, maxFit_idx] = max(fitVals);
ARX_model_params = params(maxFit_idx,:);
ARX_model = models{maxFit_idx};
ARX_model.Report.Fit
ARX_model.Report.InitialCondition

%% Wavelet
wavelet_NARX =  nlarx(dataTrain,ARX_model_params);

%% Sigmoid
sigmoid_NARX = nlarx(dataTrain,ARX_model_params,sigmoidnet);

%% Neural network
ff = feedforwardnet([5 20]);
ff.layers{2}.transferFcn = 'softmax';
ff.trainParam.epochs = 50;
ff.divideFcn = 'divideblock';

NN_NARX = nlarx(dataTrain, ARX_model_params, neuralnet(ff));


%% Figures
% figure; compare(dataTrain, ARX_model); title('Linear ARX');
% figure; compare(dataTrain, wavelet_NARX); title('Sigmoid');
% figure; compare(dataTrain, sigmoid_NARX); title('Wavelet');
% figure; compare(dataVal, NN_NARX); title('Neural network');

% Wavelet ARX plot
figure;
subplot(3,1,1);
compare(dataTrain,wavelet_NARX); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,wavelet_NARX); ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,wavelet_NARX); xlabel('time'); ylabel('amplitude'); title('Test data');

a1 = axes;
t1 = title({'Nonlinear ARX using wavelet network';''});
a1.Visible = 'off'; t1.Visible = 'on'; 

% Sigmoid ARX plot
figure;
subplot(3,1,1);
compare(dataTrain,sigmoid_NARX); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,sigmoid_NARX); ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,sigmoid_NARX); xlabel('time'); ylabel('amplitude'); title('Test data');

a2 = axes;
t2 = title({'Nonlinear ARX using sigmoid network';''});
a2.Visible = 'off'; t2.Visible = 'on'; 

% Neural network ARX plot
figure;
subplot(3,1,1);
compare(dataTrain,NN_NARX); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,NN_NARX); ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,NN_NARX); xlabel('time'); ylabel('amplitude'); title('Test data');

a3 = axes;
t3 = title({'Nonlinear ARX using neural network';''});
a3.Visible = 'off'; t3.Visible = 'on'; 

% All models comparison
figure;
compare(dataTest, ARX_model, wavelet_NARX, sigmoid_NARX, NN_NARX);
ylabel('amplitude'); title('Model comparison on test data');







