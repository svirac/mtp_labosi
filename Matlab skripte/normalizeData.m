function outArray = normalizeData(inArray)

outArray = zeros(length(inArray),1);

lbound = min(inArray);
ubound = max(inArray);

for i=1:length(inArray)
    if isnan(inArray(i))
        continue
    end
   outArray(i) = (inArray(i)-lbound) / (ubound-lbound);
end


end