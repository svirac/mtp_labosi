close all;
clear;
clc;

%% Load data
filename = 'data/bodydata.csv';
dataset = csvread(filename);
n = length(dataset(:,1));

%% PCA - task 1 -----------------------------------------------------------
% PCA for features 1 (biacromial diameter),8 (bicep girth) 
% and 16 (height)
data = dataset(:,[1,8,16]);
[coeff,score,latent,tsquared,explained,mu] = pca(data);

p1 = [-25*coeff(:,1)'; 25*coeff(:,1)'];
p1_T = p1 + ones(2,1)*mu;
p2 = [-10*coeff(:,2)'; 10*coeff(:,2)'];
p2_T = p2 + ones(2,1)*mu;
p3 = [-5*coeff(:,3)'; 5*coeff(:,3)'];
p3_T = p3 + ones(2,1)*mu;

% Projection directions with original data
figure; plot3(data(:,1),data(:,2),data(:,3),'.');
grid;
xlabel('Biacromial diameter');
ylabel('Bicep girth');
zlabel('Height');
title('Projection directions with original data - Task 1');
hold on; plot3(p1_T(:,1),p1_T(:,2),p1_T(:,3),'r');
hold on; plot3(p2_T(:,1),p2_T(:,2),p2_T(:,3),'g');
hold on; plot3(p3_T(:,1),p3_T(:,2),p3_T(:,3),'b');

% Projection directions with projeciton data
figure; plot3(score(:,1),score(:,2),score(:,3),'.');
title('Projection directions with projection data - Task 1');
grid;
hold on;
plot3(p1(:,1),p1(:,2),p1(:,3),'r');
plot3(p2(:,1),p2(:,2),p2(:,3),'g');
plot3(p3(:,1),p3(:,2),p3(:,3),'b');

figure;
pareto(explained);
ylabel('Variance explained');
xlabel('Principal component');
title('Pareto diagram - Task 1');


%% PCA - task 2 -----------------------------------------------------------
% PCA for first 13 features
[coeff,score,latent,tsquared,explained,mu] = pca(dataset(:,1:13));

figure;
pareto(explained);
ylabel('Variance explained');
xlabel('Principal component');
title('Pareto diagram - Task 2');


%% Kmeans - taks 3  -------------------------------------------------------
% kmeans for features 6 (hip girth) and 15 (weight)
data = dataset(:,[6,15]);

% Evaluate optimal number of clusters
clust = zeros(n,6);
for i=1:6
   clust(:,i) =  kmeans(data,i,'Distance','cosine');
end
va = evalclusters(data,clust,'silhouette', 'Distance','cosine');
idx = clust(:,va.OptimalK);

group = dataset(:,17);
figure;  gscatter(dataset(:,6),dataset(:,15),group,'rb','.',10);
hold on; plot(data(idx==1,1),data(idx==1,2),'ro','MarkerSize',4);
hold on; plot(data(idx==2,1),data(idx==2,2),'bo','MarkerSize',4);
title('Kmeans clustering - Task 3');
xlabel('Hip girth');
ylabel('Weight');
legend('Female','Male','Cluster 1','Cluster 2');


%% Fit Gaussian mixture model - task 3 ------------------------------------
GMModel = fitgmdist(data,va.OptimalK);
idxGM = cluster(GMModel, data);

group = dataset(:,17);
figure;  gscatter(dataset(:,6),dataset(:,15),group,'rb','.',10);
hold on; plot(data(idxGM==1,1),data(idxGM==1,2),'ro','MarkerSize',4);
hold on; plot(data(idxGM==2,1),data(idxGM==2,2),'bo','MarkerSize',4);
title('Gaussian mixture model clustering - Task 3');
xlabel('Hip girth');
ylabel('Weight');
legend('Female','Male','Cluster 1','Cluster 2');


%% Kmeans - taks 4 --------------------------------------------------------
% kmeans for features 3 (chest girth), 4 (waist girth) and 15 (weight)
data2 = dataset(:,[3,4,15]);
clust2 = zeros(n,6);
for i=1:6
   clust2(:,i) =  kmeans(data2,i,'Distance','sqeuclidean');
end
va2 = evalclusters(data2,clust2,'silhouette','Distance','sqeuclidean');
idx2 = clust2(:,va2.OptimalK);

figure;
plot3(dataset((dataset(:,17)==0),3),dataset((dataset(:,17)==0),4), ...
      dataset((dataset(:,17)==0),15), ...
      'ro','MarkerSize',3,'MarkerFaceColor','r');
hold on;
plot3(dataset((dataset(:,17)==1),3),dataset((dataset(:,17)==1),4), ...
      dataset((dataset(:,17)==1),15), ...
      'bo','MarkerSize',3,'MarkerFaceColor','b');
hold on;
plot3(data2(idx2==1,1),data2(idx2==1,2),data2(idx2==1,3), ...
      'ro','MarkerSize',4);
hold on; 
plot3(data2(idx2==2,1),data2(idx2==2,2),data2(idx2==2,3), ...
      'bo','MarkerSize',4);
title('Kmeans clustering - Task 4');
xlabel('Chest girth');
ylabel('Waist girth');
zlabel('Weight');
legend('Female','Male','Cluster 1','Cluster 2');


%% Fit Gaussian mixture model - task 4 ------------------------------------
GMModel2 = fitgmdist(data2,va2.OptimalK);
idxGM = cluster(GMModel2, data2);

figure;
plot3(dataset((dataset(:,17)==0),3),dataset((dataset(:,17)==0),4), ...
      dataset((dataset(:,17)==0),15), ...
      'ro','MarkerSize',3,'MarkerFaceColor','r');
hold on;
plot3(dataset((dataset(:,17)==1),3),dataset((dataset(:,17)==1),4), ...
      dataset((dataset(:,17)==1),15), ...
      'bo','MarkerSize',3,'MarkerFaceColor','b');
hold on;
plot3(data2(idxGM==1,1),data2(idxGM==1,2),data2(idxGM==1,3), ...
      'ro','MarkerSize',4);
hold on; 
plot3(data2(idxGM==2,1),data2(idxGM==2,2),data2(idxGM==2,3), ...
      'bo','MarkerSize',4);
title('Gaussian mixture model clustering - Task 4');
xlabel('Chest girth');
ylabel('Waist girth');
zlabel('Weight');
legend('Female','Male','Cluster 1','Cluster 2');






