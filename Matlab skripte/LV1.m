clear;
close all;
clc;

%% Load data
% filename = 'data/lv1_ident.txt';
filename = 'data/identData_15.txt';

A = load(filename);
t = A(:,1);
u = A(:,2);
y = A(:,3);
ts = 0.2;
n = length(y);

figure;
title('1');
subplot(2,1,1);
plot(t,u);
xlabel('time'); ylabel('amplitude'); title('Input data');
subplot(2,1,2);
plot(t,y);
xlabel('time'); ylabel('amplitude'); title('Output data');


%% Normalize and standardize data
y_normal = normalizeData(y);
[y_standard, ~, sigma] = standardizeData(y); % sigma - standard deviation


%% Estimate missing data
y_est = y;
u_est = u;
nans = [];
for i=2:(n-1)
    if(isnan(y(i)))
        y_est(i) = (y(i-1) + y(i+1)) / 2;
        nans = [nans
                t(i) y_est(i)];
    end
    if(isnan(u(i)))
        u_est(i) = (u(i-1) + u(i+1)) / 2;
    end
end

figure;
subplot(2,1,1);
plot(t,y);
hold on;
plot(nans(:,1), nans(:,2), 'o');
title('Detected missing data');
subplot(2,1,2);
plot(t,y_est);
title('Estimated missing data');


%% Detect outliers and estimate new values
outliers = [];
for i=2:(n-1)
    mean = (y_standard(i-1)+y_standard(i+1)) / 2;
    if (y_standard(i) > (mean+sigma*0.7) || y_standard(i) < (mean-sigma*0.7))
        y_est(i) = (y(i-1) + y(i+1)) / 2;
        outliers = [outliers
                    t(i) y(i)];
    end      
end

% My data (identData_15) has no outliers
if(~isempty(outliers))
    figure;
    subplot(2,1,1);
    plot(t,y);
    hold on;
    plot(outliers(:,1), outliers(:,2), 'o');
    title('Detected outliers')

    subplot(2,1,2);
    plot(t,y_est);
    title('Estimated outliers')
end

A_est = [t, u_est, y_est];


%% Detrend data
A_id = iddata(y_est, u_est, ts);

A_id_meanTrend = getTrend(A_id,0);
A_id_linTrend= getTrend(A_id,1);

trend_mean = detrend(A_id, 0);
trend_linear = detrend(A_id,1);

figure;
plot(A_id);
hold on;
plot(trend_mean, 'r');
title('Mean detrend');
legend('originial','detrend','location','best');

figure;
plot(A_id);
hold on;
plot(trend_linear, 'r');
title('Linear fit detrend');
legend('originial','detrend','location','best');


%% Divide data for traning and testing
splitIdx = round(n*0.8);
trainData = zeros(splitIdx,3);
testData = zeros(n-splitIdx,3);

for i=1:splitIdx
    trainData(i,:) = A_est(i,:);
end

for i=1:n-splitIdx
   testData(i,:) = A_est(i+splitIdx,:);
end    

trainData_id = iddata(trainData(:,3), trainData(:,2), ts);
testData = iddata(testData(:,3), testData(:,2), ts);