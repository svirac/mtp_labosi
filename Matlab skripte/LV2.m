clear;
close all;
clc;

%% Load data
filename= 'data/identData_complete.csv';
% Data got from LV1 - without ANs and outliers
data = csvread(filename);
t = data(:,1);
u = data(:,2);
y = data(:,3);
ts = 0.2;
n = length(y);

data_id = iddata(y, u, ts);
% Detrend
radna_tocka = 1.5;
data_T = iddata(y-radna_tocka, u-u(1), ts);

% Divide data
[trainInd, valInd, testInd] = divideblock(n,0.7,0.15,0.15);
dataTrain = data_T(trainInd);
dataVal = data_T(valInd);
dataTest = data_T(testInd);


%% ARX
A = 1:5;
B = 1:5;
K = 1:5;
n_models = length(A)*length(B)*length(K);
models = cell(1,n_models);
fitVals = zeros(1,n_models);
params = zeros(n_models, 3);
count = 1;
for i = 1:length(A)
    for j = 1:length(B)
        for k = 1:length(K)
            
            params(count,:) = [A(i) B(j) K(k)];
            models{count} = arx(dataTrain, params(count,:));           
            [valY, valFit] = compare(dataVal,models{count});
            fitVals(count) = valFit;
            
            count = count+1;
        end
    end
end

[~, maxFit_idx] = max(fitVals);
ARX_model_params = params(maxFit_idx,:);
ARX_model = models{maxFit_idx};
ARX_model.Report.Fit
ARX_model.Report.InitialCondition


%% ARMAX
A = 1:5;
B = 1:5;
C = 1:5;
K = 1:5;
n_models = length(A)*length(B)*length(C)*length(K);
models = cell(1,n_models);
fitVals = zeros(1,n_models);
params = zeros(n_models, 4);
count = 1;
for i = 1:length(A)
    for j = 1:length(B)
        for k =1:length(C)
            for l = 1:length(K)

                params(count,:) = [A(i) B(j) C(k) K(l)];
                models{count} = armax(dataTrain, params(count,:));
                [valY, valFit] = compare(dataVal,models{count});
                fitVals(count) = valFit;

                count= count+1;
            end
        end
    end
end

[~, maxFit_idx] = max(fitVals);
ARXMAX_model_params = params(maxFit_idx,:);
ARXMAX_model = models{maxFit_idx};
ARXMAX_model.Report.Fit
ARXMAX_model.Report.InitialCondition


%% Box-Jenkins
B = 1:3;
C = 1:3;
D = 1:3;
F = 1:3;
K = 1:3;
n_models = length(B)*length(C)*length(D)*length(F)*length(K);
models = cell(1,n_models);
fitVals = zeros(1,n_models);
params = zeros(n_models, 5);
count = 1;
for i = 1:length(B)
    for j = 1:length(C)
        for k =1:length(D)
            for l = 1:length(F)
                for m = 1:length(K)

                    params(count,:) = [B(i) C(j) D(k) F(l) K(m)];
                    models{count} = bj(dataTrain, params(count,:));
                    [valY, valFit] = compare(dataVal,models{count});
                    fitVals(count) = valFit;

                    count= count+1;
                end
            end
        end
    end
end

[~, maxFit_idx] = max(fitVals);
BJ_model_params = params(maxFit_idx,:);
BJ_model = models{maxFit_idx};
BJ_model.Report.Fit
BJ_model.Report.InitialCondition


%% Plots
figure;
plot(data_id);
hold on;
plot(data_T, 'r');
title('Detrend');
legend('originial','detrend','location','best');

% ARX plot
figure;
subplot(3,1,1);
compare(dataTrain,ARX_model); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,ARX_model);  ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,ARX_model); xlabel('time'); ylabel('amplitude'); title('Test data');

a1 = axes;
t1 = title({'ARX';''});
a1.Visible = 'off'; t1.Visible = 'on';

% ARMAX plot
figure;
subplot(3,1,1);
compare(dataTrain,ARXMAX_model); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,ARXMAX_model); ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,ARXMAX_model); xlabel('time'); ylabel('amplitude'); title('Test data');

a2 = axes;
t2 = title({'ARMAX';''});
a2.Visible = 'off'; t2.Visible = 'on'; 

% Box-JeKins plot
figure;
subplot(3,1,1);
compare(dataTrain,BJ_model); ylabel('amplitude'); title('Train data');
subplot(3,1,2);
compare(dataVal,BJ_model); ylabel('amplitude'); title('Validation data');
subplot(3,1,3);
compare(dataTest,BJ_model); xlabel('time'); ylabel('amplitude'); title('Test data');

a3 = axes;
t3 = title({'BOX-JEKINS';''});
a3.Visible = 'off'; t3.Visible = 'on'; 









