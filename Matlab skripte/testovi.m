close all;
clear;
clc;

%% Load data
filename = 'data/bodydata.csv';
dataset = csvread(filename);
n = length(dataset(:,1));

group = dataset(:,17);
figure;
gscatter(dataset(:,6),dataset(:,15),group);