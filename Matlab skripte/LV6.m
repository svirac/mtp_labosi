close all;
clear;
clc;

%% Load and data
filename = 'data/bodydata.csv';
dataset = csvread(filename);
n = length(dataset(:,1));

% Divide into training and test data
rng(10,'twister');
[trainInd, ~, testInd] = dividerand(n, 0.8, 0, 0.2);

% Set columns for train data and y column for classification and regression
trainCols = 1:13;
genderCol = 17;
weightCol = 15;


%% PCA and data standardization
[coeff,score,latent,tsquared,explained,mu] = pca(dataset(:,trainCols));
% figure;
% pareto(explained);
% ylabel('Variance explained');
% xlabel('Principal component');
% title('Pareto diagram');

% First five features explain more than 95% information
trainData = score(trainInd, 1:5);
testData = score(testInd, 1:5);

% Standardize data
[trainDataStd,trainMean,trainSigma] = standardizeData(trainData);
testDataStd = standardizeData(testData,trainMean,trainSigma);


%% Classification parameters
X_train = trainDataStd;
y_train = dataset(trainInd, genderCol);

X_test = testDataStd;
y_test = dataset(testInd, genderCol);

n_train_data = length(trainDataStd);

max_knn = 40;
kernel_scale = 0.1:0.1:3;

kfold = 10;
kfold_size = floor(n_train_data/kfold);


%% Task 1 - KNN classification model
err_knn = zeros(max_knn,1);
for i_knn=1:max_knn
    
    err_kfold = zeros(kfold,1);
    for j=1:kfold
        
        % Get indices for test fold group
        indices = j+(j-1)*kfold_size : j+j*kfold_size;
        indices(indices>=n_train_data) = [];
        
        X_train_loc = trainDataStd;
        X_train_loc(indices,:) = [];
        
        y_train_loc = dataset(trainInd,genderCol);
        y_train_loc(indices,:) = [];
        
        X_test_loc = trainDataStd(indices,:);
        
        pom = dataset(trainInd,genderCol);
        y_test_loc = pom(indices,:);
        
        KNN_model = fitcknn(X_train_loc,y_train_loc,...
                            'NumNeighbors',i_knn,...
                            'Distance','euclidean',...
                            'ClassNames',[0;1]);
                        
        y_predict = predict(KNN_model,X_test_loc);
        
        err_kfold(j) = sum(abs(y_test_loc - y_predict));      
    end
    
    err_knn(i_knn) = sum(err_kfold/kfold);
end


%% Plot MAE and CFM for KNN model
[min_err_knn, ind_best_knn] = min(err_knn);

figure; plot(err_knn);
title('K nearest neighbors - MAE for number of nearest neighbors and Kfold=10');
xlabel('Number of nearest neighbors');
ylabel('MAE');
hold on;
plot(ind_best_knn,min_err_knn,'ro');

% Best KNN model
best_KNN_model = fitcknn(X_train,y_train,...
                        'NumNeighbors',ind_best_knn,...
                        'Distance','euclidean',...
                        'ClassNames',[0;1]);
                        
y_predict = predict(best_KNN_model, X_test);
cfm_knn = confusionmat(y_test, y_predict);
figure;
plotconfusion(y_test', y_predict');
title('KNN model - confusion matrix');


%% Task 2 - SVM classification model
err_svm_class = zeros(length(kernel_scale),1);
for i=1:length(kernel_scale)
    i_svm = kernel_scale(i);
    err_kfold = zeros(kfold,1);
    for j=1:kfold
        
        % Get indices for test Kfold
        indices = j+(j-1)*kfold_size : j+j*kfold_size;
        indices(indices>=n_train_data) = [];
        
        X_train_loc = trainDataStd;
        X_train_loc(indices,:) = [];
        
        y_train_loc = dataset(trainInd,genderCol);
        y_train_loc(indices,:) = [];
        
        X_test_loc = trainDataStd(indices,:);
        
        pom = dataset(trainInd,genderCol);
        y_test_loc = pom(indices,:);
        
        SVM_class_model = fitcsvm(X_train_loc,y_train_loc,...
                            'KernelFunction','gaussian',...
                            'PolynomialOrder',[],...
                            'KernelScale',i_svm,...
                            'BoxConstraint',1,...
                            'ClassNames',[0;1]);
                        
        y_predict = predict(SVM_class_model,X_test_loc);
        
        err_kfold(j) = sum(abs(y_test_loc - y_predict));      
    end
    
    err_svm_class(i) = sum(err_kfold)/kfold;
end

%% Plot MAE and CFM for SVM model
[min_err_svm_class, ind_best_svm_class] = min(err_svm_class);

figure; plot(err_svm_class);
title('SVM classification model - MAE for kernel size and Kfold=10');
xlabel('Kernel size');
ylabel('MAE');
hold on;
plot(ind_best_svm_class, min_err_svm_class,'ro');

% Best SVM model
best_SVM_class_model = fitcsvm(X_train,y_train,...
                        'KernelFunction','gaussian',...
                        'PolynomialOrder',[],...
                        'KernelScale',ind_best_svm_class,...
                        'BoxConstraint',1,...
                        'ClassNames',[0;1]);
                        
y_predict = predict(best_SVM_class_model, X_test);
cfm_svm_class = confusionmat(y_test, y_predict);
figure;
plotconfusion(y_test', y_predict');
title('SVM model - confusion matrix');


%% Regression parameters
y_dataStd = standardizeData(dataset(:,15));

X_train = trainDataStd;

y_train = y_dataStd(trainInd, :);

X_test = testDataStd;
y_test = y_dataStd(testInd, :);

%% Task 3 - Generalized linear regression model
GLR_model = fitglm(X_train, y_train);

y_predict = predict(GLR_model, X_test);

figure;
plot(1:length(y_predict),y_predict);
hold on;
plot(1:length(y_test),y_test);
title('Generalized linear model - prediction and ground truth comparison');
ylabel('Weight [kg]');
xlabel('Data indices');
legend('Predicted data','Ground truth', 'Location','northeast');

err_glr = mae(y_predict, y_test);


%% Task 4 - SVM regression model 
err_svm_reg = zeros(length(kernel_scale),1);
for i=1:length(kernel_scale)
    i_svm = kernel_scale(i);
    
    err_kfold = zeros(kfold,1);
    for j=1:kfold
        
        % Get indices for test Kfold
        indices = j+(j-1)*kfold_size : j+j*kfold_size;
        indices(indices>=n_train_data) = [];      
        
        X_train_loc = trainDataStd;
        X_train_loc(indices,:) = [];
        
        y_train_loc = dataset(trainInd,weightCol);
        y_train_loc(indices,:) = [];
        
        X_test_loc = trainDataStd(indices,:);
        
        pom = dataset(trainInd,weightCol);
        y_test_loc = pom(indices,:);
        
        SVM_reg_model = fitrsvm(X_train_loc,y_train_loc,...
                                'KernelScale',i_svm);
                        
        y_predict = predict(SVM_reg_model,X_test_loc);

        err_kfold(j) = sum(abs(y_test_loc - y_predict));     
    end
    
    err_svm_reg(i) = sum(err_kfold)/kfold;
end

%% Plot MAE and CFM for SVM model
[min_err_svm_reg, ind_best_svm_reg] = min(err_svm_reg);

figure; plot(err_svm_reg);
title('SVM regression model - MAE for kernel size and Kfold=10');
xlabel('Kernel size');
ylabel('MAE');
hold on;
plot(ind_best_svm_reg, min_err_svm_reg,'ro');
% Best SVM model
best_SVM_reg_model = fitrsvm(X_train,y_train,...
                            'KernelScale',ind_best_svm_reg);
                        

y_predict = predict(best_SVM_reg_model, X_test);

figure;
plot(1:length(y_predict),y_predict);
hold on;
plot(1:length(y_test),y_test);
title('SVM model - prediction and ground truth comparison');
ylabel('Weight [kg]');
xlabel('Data indices');
legend('Predicted data','Ground truth', 'Location','northeast');

mae_svm_reg = mae(y_predict, y_test);




